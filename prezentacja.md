---
author: Aleksandra Kurdel-Lizer
title: Parki Narodowe Stanów Zjednoczonych
date: 24.04.2024
theme: Szeged
output: beamer_presentation
toc: true
header-includes: \usepackage[dvipsnames]{xcolor} \usepackage{graphicx}
---

# Wstęp

## \textcolor{SeaGreen}{Parki Narodowe Stanów Zjednoczonych}

Parki narodowe Stanów Zjednoczonych to rozległa sieć chronionych terenów, obejmujących różnorodne ekosystemy i oferujących szereg możliwości rekreacji, edukacji i ochrony przyrody. Te skarby narodowe są dostępne dla wszystkich i stanowią świadectwo bogatego dziedzictwa kulturowego i przyrodniczego kraju.

![Zdjęcie Parku Narodowego Yosemite](./images/Park%20Narodowy%20Yosemite.jpeg){ height=50% }\

## \textcolor{Peach}{Najpopularniejsze parki narodowe}

\begin{columns}
\begin{column}{0.5\textwidth}

\begin{itemize}
\item Park Narodowy Yellowstone: Znany z gejzerów, gorących źródeł i zbiorników błotnych.
\item Park Narodowy Yosemite: Słynie z granitowych klifów, wodospadów i sekwoi olbrzymich.
\item Park Narodowy Wielkiego Kanionu: Jedno z siedmiu cudów natury świata.
\item Park Narodowy Glacier: Góry, lodowce i alpejskie jeziora.
\end{itemize}

\end{column}
\begin{column}{0.5\textwidth}
\includegraphics{./images/12-Most-Beautiful-National-Parks-in-the-USA.jpeg}
\end{column}
\end{columns}

# Dane

## Trochę danych

| Park Narodowy | Rok Założenia | Obszar (akry) | Liczba Odwiedzających (2021) |
| ------------- | :-----------: | ------------: | ---------------------------: |
| Acadia        |     1919      |        49,076 |                    2,791,034 |
| Badlands      |     1978      |       242,756 |                    1,071,152 |
| Big Bend      |     1944      |       801,163 |                      448,203 |
| Biscayne      |     1980      |       172,924 |                      563,259 |
| Bryce Canyon  |     1928      |        35,835 |                    2,073,579 |
| Capitol Reef  |     1971      |       241,904 |                    1,129,177 |

# Ciekawostki

## \textcolor{SeaGreen}{Różnorodność parków narodowych}

Parki narodowe USA obejmują szeroki zakres ekosystemów, w tym:

- Góry: Park Narodowy Yosemite, Park Narodowy Denali
- Pustynie: Park Narodowy Doliny Śmierci, Park Narodowy Joshua Tree
- Lasy: Park Narodowy Sekwoi, Park Narodowy Acadia
- Wybrzeża: Park Narodowy Everglades, Park Narodowy Acadia
- Jeziora: Park Narodowy Kraterowe Jezioro, Park Narodowy Jeziora Yellowstone

\begin{block}{Ciekawostka}
Park Narodowy Yellowstone to jedyny park narodowy na świecie, który posiada cały spektrum ekosystemów, od górskich szczytów po gorące źródła termalne, co czyni go niezwykłym miejscem badawczym dla naukowców z całego świata.
\end{block}

## \textcolor{Peach}{Wyzwania}

\begin{columns}

\begin{column}{0.5\textwidth}
\includegraphics{./images/national_park_trashes.jpeg}
\end{column}

\begin{column}{0.5\textwidth}

\begin{itemize}
\item Parki narodowe USA stoją przed wieloma wyzwaniami, w tym zmianami klimatu, zanieczyszczeniem i nadmierną turystyką.
\item Zmiany klimatu powodują, że parki narodowe stają się coraz bardziej suche i podatne na pożary.
\item Nadmierna turystyka może prowadzić do degradacji ekosystemów parków narodowych.
\end{itemize}

\end{column}
\end{columns}
